*** Setting ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${testUrl}      ${scheme}://${homepage}

*** Keywords ***
Open homepage
    Open browser    ${testUrl}  ${browser}

*** Test cases ***
C001 Hacer clic en contenedores
    Open homepage
    Set Global Variable    @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombreDeContenedor}   IN      @{nombresDeContenedores}
    \   click element       xpath=${nombreDeContenedor}
    \   wait until element is visible       xpath=//*[@id="bigpic"]
    \   click element       //*[@id="header_logo"]/a/img
    close browser
C002 Caso de prueba nuevo
    Open homepage
    Close browser